package com.sparkProject

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.ml.feature._
import org.apache.spark.ml.classification.LogisticRegression
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.ml.tuning.TrainValidationSplit
import org.apache.spark.ml.tuning.ParamGridBuilder
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.ml.Pipeline
import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object Trainer {

  def main(args: Array[String]): Unit = {

    val conf = new SparkConf().setAll(Map(
      "spark.scheduler.mode" -> "FIFO",
      "spark.speculation" -> "false",
      "spark.reducer.maxSizeInFlight" -> "48m",
      "spark.serializer" -> "org.apache.spark.serializer.KryoSerializer",
      "spark.kryoserializer.buffer.max" -> "1g",
      "spark.shuffle.file.buffer" -> "32k",
      "spark.default.parallelism" -> "12",
      "spark.sql.shuffle.partitions" -> "12",
      "spark.driver.maxResultSize" -> "2g"
    ))

    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    val spark = SparkSession
      .builder
      .config(conf)
      .appName("TP_spark")
      .getOrCreate()


    /*******************************************************************************
      *
      *       TP 4-5
      *
      *       - lire le fichier sauvegarder précédemment
      *       - construire les Stages du pipeline, puis les assembler
      *       - trouver les meilleurs hyperparamètres pour l'entraînement du pipeline avec une grid-search
      *       - Sauvegarder le pipeline entraîné
      *
      *       if problems with unimported modules => sbt plugins update
      *
      ********************************************************************************/

   /** CHARGER LE DATASET **/
    val df = spark.read.parquet("./prepared_trainingset")

   /** DEFINITION DES DIFFERENTS STAGES**/
    /** TF-IDF: Ce stage convertit les chaînes de caractères en liste de mots**/
    val tokenizer = new RegexTokenizer()
      .setPattern("\\W+")
      .setGaps(true)
      .setInputCol("text")
      .setOutputCol("tokens_raw")

    /** Enlève les mots qui n'on pas de valeurs significatives comme les prépositions, les déterminants, etc.**/
    val remover = new StopWordsRemover()
      .setInputCol("tokens_raw")
      .setOutputCol("tokens")

    /** "Term Frequency" **/
    val vectorizer = new CountVectorizer()
      .setInputCol("tokens")
      .setOutputCol("TF")

    /** "Inverse Document Frequency" **/
    val idf = new IDF()
      .setInputCol("TF")
      .setOutputCol("tfidf")
      .setMinDocFreq(2)

    /** Remplace les valeurs catégorielles de la variable "country2" par des valeurs numériques **/
    val strIndCountry = new StringIndexer()
      .setInputCol("country2")
      .setOutputCol("country_indexed")
      .setHandleInvalid("skip")

    /** Remplace les valeurs catégorielles de la variable "currency2" par des valeurs numériques **/
    val strIndCurrency = new StringIndexer()
      .setInputCol("currency2")
      .setOutputCol("currency_indexed")
      .setHandleInvalid("skip")


    /** VECTOR ASSEMBLER **/
    val vecAss = new VectorAssembler()
      .setInputCols(Array("tfidf", "days_campaign", "hours_prepa", "goal", "country_indexed", "currency_indexed"))
      .setOutputCol("features")


    /** MODEL **/
    val lr = new LogisticRegression()
      .setElasticNetParam(0.0)
      .setFitIntercept(true)
      .setFeaturesCol("features")
      .setLabelCol("final_status")
      .setStandardization(true)
      .setPredictionCol("predictions")
      .setRawPredictionCol("raw_predictions")
      .setThresholds(Array(0.7, 0.3))
      .setTol(1.0e-6)
      .setMaxIter(300)


    /** PIPELINE **/
    val pipeline = new Pipeline()
      .setStages(Array(tokenizer, remover, vectorizer, idf, strIndCountry, strIndCurrency, vecAss, lr))


    /** TRAINING AND GRID-SEARCH **/
    val Array(training, test) = df.randomSplit(Array(0.9, 0.1))

    val paramGrid = new ParamGridBuilder()
      .addGrid(lr.regParam, Array(0.00000001, 0.000001, 0.0001, 0.01))
      .addGrid(vectorizer.minDF, Array(55.0, 75.0, 95.0))
      .build()

    /* Crée un modèle de classification multiclasse. Son apprentissage sera supervisé par la colonne "final_status" (le
     label, sa prédiction est écrite dans une nouvelle colonne "prédictions"
    */
    val evaluator = new MulticlassClassificationEvaluator()
      .setPredictionCol("predictions")
      .setLabelCol("final_status")

    /* Méthode de recherche du meilleur modèle en validation croisée à l'aide de la pipeline, de la grille de paramètres
    et de la famille de modèle précedemment définis
    */
    val valid = new TrainValidationSplit()
      .setEstimator(pipeline)
      .setEstimatorParamMaps(paramGrid)
      .setEvaluator(evaluator)
      .setTrainRatio(0.7)

    /* Entraînement du modèle sur le set dédié */
    val model = valid.fit(training)
    /* Crée un dataframe avec les prédictions sur le set de test à partir du meilleur modèle (qui a maximisé le score
     lors de la validation croisée)
    */
    val df_WithPredictions = model.bestModel.transform(test)
    /* F1-score obtenu sur le set de test */
    val score = evaluator.evaluate(df_WithPredictions)

    /* Affiche les 20 premières lignes du Dataframe avec les prédictions */
    df_WithPredictions.show(20)
    println(s"F1 score = $score")

    /* Sauvegarde du modèle */
    /*model.save("./model_trainer")*/

    /* Affiche un tableau montrant les vrais et faux positifs et négatifs */
    df_WithPredictions.groupBy("final_status", "predictions").count.show()
  }

}
